layThongTinTuForm = () => {
  var ma = document.getElementById("txtMaSV").value;
  var ten = document.getElementById("txtTenSV").value;
  var email = document.getElementById("txtEmail").value;
  var matKhau = document.getElementById("txtPass").value;
  var toan = document.getElementById("txtDiemToan").value * 1;
  var ly = document.getElementById("txtDiemLy").value * 1;
  var hoa = document.getElementById("txtDiemHoa").value * 1;
  var sv = new SinhVien(ma, ten, email, matKhau, toan, ly, hoa);
  return sv;
};

handleTalbe = (array) => {
  var contentHTML = "";
  for (i = 0; i < array.length; i++) {
    var sv = array[i];
    var contentTr = `<tr>
        <td>${sv.ma}</td>
        <td>${sv.ten}</td>
        <td>${sv.email}</td>
        <td>${sv.dtb()}</td>
        <td>
          <button onclick="suaSV(${sv.ma})" class="btn btn-primary">Sửa</button>
          <button onclick="xoaSV(${sv.ma})" class="btn btn-danger">Xóa</button>
        </td>
    </tr>`;
    contentHTML += contentTr;
  }
  document.getElementById("tbodySinhVien").innerHTML = contentHTML;
};

showThongTinLenForm = (sv) => {
  document.getElementById("txtMaSV").value = sv.ma;
  document.getElementById("txtTenSV").value = sv.ten;
  document.getElementById("txtEmail").value = sv.email;
  document.getElementById("txtPass").value = sv.password;
  document.getElementById("txtDiemToan").value = sv.toan;
  document.getElementById("txtDiemLy").value = sv.ly;
  document.getElementById("txtDiemHoa").value = sv.hoa;
};

resetForm = () => {
  document.getElementById("formQLSV").reset();
};
