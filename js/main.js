dsSv = [];

// lấy JSON lên khi user load trang
var dataJson = localStorage.getItem("DSSV_LOCAL");
// convert ngược lại từ JSON về array
if (dataJson != null) {
  var dataArr = JSON.parse(dataJson);
  // JSON.parse(dataJson) sẽ return về null  hoặc array
  for (var i = 0; i < dataArr.length; i++) {
    var item = dataArr[i];
    var sv = new SinhVien(
      item.ma,
      item.ten,
      item.email,
      item.matKhau,
      item.toan,
      item.ly,
      item.hoa
    );
    dsSv.push(sv);
  }
  handleTalbe(dsSv);
}

themSinhVien = () => {
  var sv = layThongTinTuForm();
  dsSv.push(sv);
  var dataJson = JSON.stringify(dsSv);
  localStorage.setItem("DSSV_LOCAL", dataJson);
  handleTalbe(dsSv);
};

xoaSV = (id) => {
  var locate = -1;
  for (var i = 0; i < dsSv.length; i++) {
    var sv = dsSv[i];
    if (sv.ma == id) {
      locate = i;
    }
  }
  dsSv.splice(locate, 1);
  handleTalbe(dsSv);
};

suaSV = (id) => {
  var locate = dsSv.findIndex((item) => {
    return item.ma == id;
  });
  if (locate != -1) {
    document.getElementById("txtMaSV").disabled = true;
    showThongTinLenForm(dsSv[locate]);
  }
};

capNhatSinhVien = () => {
  document.getElementById("txtMaSV").disabled = false;
  var sv = layThongTinTuForm();

  var locate = dsSv.findIndex((item) => {
    return sv.ma == item.ma;
  });
  if (locate !== -1) {
    dsSv[locate] = sv;
    handleTalbe(dsSv);
    resetForm();
  }
};
